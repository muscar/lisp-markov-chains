;;;; package.lisp

(defpackage #:markov
  (:use #:cl)
  (:import-from #:alexandria #:hash-table-keys)
  (:import-from #:split-sequence #:split-sequence-if))