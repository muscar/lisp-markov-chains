;;;; markov.lisp

(in-package #:markov)

;;; Utils

(defun space-char-p (c)
  (or (char= c #\Space) (char= c #\Newline)))

(defun choose (seq)
  (elt seq (random (length seq))))

(defun iter/windows (seq n f)
  (dotimes (idx (1+ (- (length seq) n)))
    (funcall f idx (+ idx n))))

(defmacro let/ret ((var-sym value-form &optional (result-form nil result-form-supplied-p)) &body forms)
  `(let ((,var-sym ,value-form))
     (prog1 ,(or (and result-form-supplied-p result-form) var-sym)
       ,@forms)))

;;; Markov chain based random text generation

(defun read-file (path)
  (with-open-file (s path)
    (let/ret (contents (make-string (file-length s)))
      (read-sequence contents s))))

(defun words (text)
  (let ((seq (split-sequence-if #'space-char-p text)))
    (make-array (length seq) :initial-contents seq)))

(defun make-word-map (words)
  (let/ret (word-map (make-hash-table :test #'equal))
    (labels ((make-key (start)
               (cons (aref words start) (aref words (1+ start))))
             (add-transition (start end)
               (declare (ignore end))
               (let* ((key (make-key start))
                      (follow-set (gethash key word-map '())))
                 (setf (gethash key word-map) (push (aref words (+ start 2)) follow-set)))))
      (iter/windows words 3 #'add-transition))))

(defun generate (word-map max)
  (do* ((key (choose (hash-table-keys word-map)) (cons (cdr key) word))
        (follow-set #1=(gethash key word-map) #1#)
        (word #2=(choose follow-set) #2#)
        (cnt 0 (1+ cnt))
        (acc '() (push (car key) acc)))
    ((or (not follow-set) (>= cnt max)) (nreverse acc))))

(defun random-text (path &optional (count 35))
  (let ((word-map (make-word-map (words (read-file path)))))
    (format t "~{~a~^ ~}" (generate word-map count))))
