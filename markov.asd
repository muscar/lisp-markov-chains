;;;; markov.asd

(asdf:defsystem #:markov
  :description "Describe markov here"
  :author "Alex Muscar"
  :license "3-Clause BSD"
  :serial t
  :components ((:file "package")
               (:file "markov"))
  :depends-on ("alexandria"
               "split-sequence"))
